package localantiguedades;

public class Cliente {
    
    private String nombreCliente;
    private String apellidoCliente;
    private String direccion;
    private String numeroCelular;

    public Cliente(String nombreCliente, String apellidoCliente, String direccion, String numeroCelular){
        this.nombreCliente = nombreCliente;
        this.apellidoCliente = apellidoCliente;
        this.direccion = direccion;
        this.numeroCelular = numeroCelular;        
    }

    public void setNombreCliente(String nombre){
        this.nombreCliente = nombre;
    }

    public String getNombreCliente(){
        return nombreCliente;
    }

    public void setApellidoCliente(String apellido){
        this.apellidoCliente = apellido;
    }

    public String getApellidoCliente(){
        return apellidoCliente;
    }

    public void setDireccion(String direccion){
        this.direccion = direccion;
    }

    public String getDireccion(){
        return direccion;
    }

    public void setNumeroCelular(String numero){
        this.numeroCelular = numero;
    }

    public String getNumeroCelular(){
        return numeroCelular;
    }
}
