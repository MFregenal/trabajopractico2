package localantiguedades;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Local {
    
    private String nombreLocal;
    private String direccion;
    private String contacto;
    private Pedido pedido;
    private String nacionalidad;
    private List<Pedido> pedidos;
    private List<Producto> stock;

    public Local(String nombreLocal, String direccion, String contacto, String nacionalidad){
        this.nombreLocal = nombreLocal;
        this.direccion = direccion;
        this.contacto = contacto;
        this.nacionalidad = nacionalidad;
        pedidos = new ArrayList<>();
        stock = new ArrayList<>();
    }

    public void setNombreLocal(String nombre){
        this.nombreLocal = nombre;
    }

    public String getNombreLocal(){
        return this.nombreLocal;
    }

    public void setDireccion(String direccion){
        this.direccion = direccion;
    }

    public String getDireccion(){
        return this.direccion;
    }

    public void setContacto(String contacto){
        this.contacto = contacto;
    }

    public String getContacto(){
        return this.contacto;
    }

    public void addStock(String nombreProducto, String pais, Integer cantidad, Double precio){
        Double impuesto;
        if (pais.equals(nacionalidad)){
            impuesto = 1.02;
        }else{
            impuesto = 1.0105;
        }
        Producto producto = new Producto(nombreProducto, pais, cantidad, precio, impuesto);
        stock.add(producto);
    }

/*    public void actualizarStock(String nombreProducto){
        for ( int i = 0; i <= stock.size(); i++){
            if (stock.get(i).getNombreProducto().equals(nombreProducto)){
                stock.get(i).setCantidad( stock.get(i).getCantidad()-1 );
            }
        }
    }
*/
/*    public Boolean stockDisponible(String nombreProducto){
        for(Producto producto : stock){
            if (producto.getNombreProducto().equals(nombreProducto)){
                if(producto.getCantidad() >= 1){
                    actualizarStock(nombreProducto);
                    return true;
                }
            }
        }
        return false;
    }
*/
    public void crearPedido(String estado, String nombreCliente, String apellidoCliente, String direccion, String numeroCelular){
        pedido = new Pedido(estado, nombreCliente, apellidoCliente, direccion, numeroCelular);
    }

    //Esta funcion busca el producto, revisa si hay stock y actualiza el stock.
    //Si no se cumple una de las 3 condiciones retorna null
    public Producto buscarProducto(String nombreProducto){
        for ( int i = 0; i <= stock.size(); i++){
            if (stock.get(i).getNombreProducto().equals(nombreProducto)){
                if(stock.get(i).getCantidad() >= 1){
                    stock.get(i).setCantidad( stock.get(i).getCantidad()-1 );
                    return stock.get(i);
                }
            }
        }
        return null;
    }

    public void addProducto(String nombreProducto){
        Producto producto = buscarProducto(nombreProducto);
        pedido.addProducto(producto.getNombreProducto(), producto.getPais(), 1, producto.getPrecio(), producto.getImpuesto());
    }

    public void addPedido(){
        pedidos.add(pedido);
    }

    public Double precioPedido(){
        Double precioTotal = 0.0;
        for ( Producto producto : pedido.getListProductos()){
            precioTotal = producto.getCantidad() * producto.getPrecio() * producto.getImpuesto();
        }
        return precioTotal;
    }

    public void pagarPedidoTarjeta(String estado, String nombreEntidad, String nombreTitular, String apellidoTitular, Integer dniTitular, String domicilioTitular, Integer numTarjeta, LocalDate FechaVencimiento, Integer numSeguridad){
        pedido.setEstado(estado);
        TarjetaCredito tarjeta = new TarjetaCredito(nombreEntidad, nombreTitular, apellidoTitular, dniTitular, domicilioTitular, numTarjeta, FechaVencimiento, numSeguridad);
        pedido.setFormaPago(tarjeta);
    }

    public void pagarPedidoCheque(String estado, String nombreEntidad, String nombreTitular, String apellidoTitular, Integer dniTitular, String domicilioTitular, Integer numOrden, String domicilioPago, Integer numCuenta, Double montoPagar){
        pedido.setEstado(estado);
        Cheque cheque = new Cheque(nombreEntidad, nombreTitular, apellidoTitular, dniTitular, domicilioTitular, numOrden, domicilioPago, numCuenta, montoPagar);
        pedido.setFormaPago(cheque);
    }
}
