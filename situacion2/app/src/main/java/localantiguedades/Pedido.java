package localantiguedades;

import java.util.ArrayList;
import java.util.List;

public class Pedido {
    
    private List<Producto> productos;
    private String estado;
    private FormaPago formaPago;
    private Cliente cliente;

    public Pedido(){

    }

    public Pedido(String estado, String nombreCliente, String apellidoCliente, String direccion, String numeroCelular){
        this.estado = estado;
        cliente = new Cliente(nombreCliente, apellidoCliente, direccion, numeroCelular);
        productos = new ArrayList<>();
    }

    public void setEstado(String estado){
        this.estado = estado;
    }

    public String getEstado(){
        return estado;
    }

    public void setFormaPago(FormaPago formaPago){
        this.formaPago = formaPago;
    }

    public FormaPago getFormaPago(){
        return formaPago;
    }

    public void addProducto(String nombreProducto, String pais, Integer cantidad, Double precio, Double impuesto){
        int band = 0;
        for ( Producto prod : productos){
            if (prod.getNombreProducto().equals(nombreProducto)){
                prod.setCantidad(prod.getCantidad()+1);
                band=1;
            }
        }
        if(band == 0){
            Producto producto = new Producto(nombreProducto, pais, cantidad, precio, impuesto);
            productos.add(producto);
        }
    }

    public List<Producto> getListProductos(){
        return productos;
    }
}
