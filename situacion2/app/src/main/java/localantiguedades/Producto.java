package localantiguedades;

public class Producto {
    
    private String nombreProducto;
    private String pais;
    private Integer cantidad;
    private Double precio;
    private Double impuesto;

    public Producto(String nombreProducto, String pais, Integer cantidad, Double precio, Double impuesto){
        this.nombreProducto = nombreProducto;
        this.pais = pais;
        this.cantidad = cantidad;
        this.precio = precio;
        this.impuesto = impuesto;
    }

    public void setNombreProducto(String nombre){
        this.nombreProducto = nombre;
    }

    public String getNombreProducto(){
        return nombreProducto;
    }

    public void setCantidad(Integer cantidad){
        this.cantidad = cantidad;
    }

    public Integer getCantidad(){
        return cantidad;
    }

    public void setPrecio(Double precio){
        this.precio = precio;
    }

    public Double getPrecio(){
        return precio;
    }

    public void setImpuesto(Double impuesto){
        this.impuesto = impuesto;
    }

    public Double getImpuesto(){
        return impuesto;
    }

    public void setPais(String pais){
        this.pais = pais;
    }

    public String getPais(){
        return pais;
    }
}
