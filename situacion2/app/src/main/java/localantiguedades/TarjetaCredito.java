package localantiguedades;

import java.time.LocalDate;

public class TarjetaCredito extends FormaPago{
    
    private Integer numTarjeta;
    private LocalDate fechaVencimiento;
    private Integer numSeguridad;

    public TarjetaCredito(String nombreEntidad, String nombreTitular, String apellidoTitular, Integer dniTitular, String domicilioTitular, Integer numTarjeta, LocalDate FechaVencimiento, Integer numSeguridad){
        super(nombreEntidad, nombreTitular, apellidoTitular, dniTitular, domicilioTitular);
        this.numTarjeta = numTarjeta;
        this.fechaVencimiento = fechaVencimiento;
        this.numSeguridad = numSeguridad;

    }
}
