package localantiguedades;

public class Cheque extends FormaPago{
    
    private Integer numOrden;
    private String domicilioPago;
    private Integer numCuenta;
    private Double montoPagar;

    public Cheque(String nombreEntidad, String nombreTitular, String apellidoTitular, Integer dniTitular, String domicilioTitular, Integer numOrden, String domicilioPago, Integer numCuenta, Double montoPagar){
        super(nombreEntidad, nombreTitular, apellidoTitular, dniTitular, domicilioTitular);
        this.numOrden = numOrden;
        this.domicilioPago = domicilioPago;
        this.numCuenta = numCuenta;
        this.montoPagar = montoPagar;
    }
}
