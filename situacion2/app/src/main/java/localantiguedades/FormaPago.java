package localantiguedades;

public class FormaPago {
    
    private String nombreEntidad;
    private String nombreTitular;
    private String apellidoTitular;
    private Integer dniTitular;
    private String domicilioTitular;

    public FormaPago(String nombreEntidad, String nombreTitular, String apellidoTitular, Integer dniTitular, String domicilioTitular){
        this.nombreEntidad = nombreEntidad;
        this.nombreTitular = nombreTitular;
        this.apellidoTitular = apellidoTitular;
        this.dniTitular = dniTitular;
        this.domicilioTitular = domicilioTitular;
    }
}
