package sistemapagos.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import sistemapagos.clases.Empresa;
import sistemapagos.clases.EmpresaContratante;
import sistemapagos.excepciones.EmpresaException;

public class VentanaRegEmpresaContratante extends javax.swing.JPanel {

    private Empresa empresa;
    private UIPrincipal principal;
    
    public VentanaRegEmpresaContratante(Empresa empresa, UIPrincipal principal) {
        this.empresa = empresa;
        this.principal = principal;
        initComponents();
        blankCampos();
        jlblAviso.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnlRegEmpresaContratante = new javax.swing.JPanel();
        jlblNombreEmpresaContratante = new javax.swing.JLabel();
        jlblCuitEmpresaContratante = new javax.swing.JLabel();
        jlblTipoServicio = new javax.swing.JLabel();
        jtfNombreEmpresa = new javax.swing.JTextField();
        jtfTipoServicio = new javax.swing.JTextField();
        jftfCuitEmpresaContratante = new javax.swing.JFormattedTextField();
        jbtnRegEmpresaContratante = new javax.swing.JButton();
        jbtnCancelar = new javax.swing.JButton();
        jlblAviso = new javax.swing.JLabel();
        jlblDireccionEmpresa = new javax.swing.JLabel();
        jtfDireccionEmpresa = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));

        jpnlRegEmpresaContratante.setBorder(javax.swing.BorderFactory.createTitledBorder("Registrar Empresa Contratante"));

        jlblNombreEmpresaContratante.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jlblNombreEmpresaContratante.setText("Nombre: ");

        jlblCuitEmpresaContratante.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jlblCuitEmpresaContratante.setText("CUIT: ");

        jlblTipoServicio.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jlblTipoServicio.setText("Tipo de Servicio: ");

        try {
            jftfCuitEmpresaContratante.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jbtnRegEmpresaContratante.setText("Registrar");
        jbtnRegEmpresaContratante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnRegEmpresaContratanteActionPerformed(evt);
            }
        });

        jbtnCancelar.setText("Cancelar");
        jbtnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnCancelarActionPerformed(evt);
            }
        });

        jlblAviso.setForeground(new java.awt.Color(255, 51, 0));
        jlblAviso.setText("jLabel1");

        jlblDireccionEmpresa.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jlblDireccionEmpresa.setText("Direccion: ");

        javax.swing.GroupLayout jpnlRegEmpresaContratanteLayout = new javax.swing.GroupLayout(jpnlRegEmpresaContratante);
        jpnlRegEmpresaContratante.setLayout(jpnlRegEmpresaContratanteLayout);
        jpnlRegEmpresaContratanteLayout.setHorizontalGroup(
            jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlRegEmpresaContratanteLayout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlRegEmpresaContratanteLayout.createSequentialGroup()
                        .addComponent(jlblAviso)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 167, Short.MAX_VALUE)
                        .addComponent(jbtnRegEmpresaContratante)
                        .addGap(18, 18, 18)
                        .addComponent(jbtnCancelar))
                    .addGroup(jpnlRegEmpresaContratanteLayout.createSequentialGroup()
                        .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlblTipoServicio)
                            .addComponent(jlblCuitEmpresaContratante)
                            .addComponent(jlblNombreEmpresaContratante)
                            .addComponent(jlblDireccionEmpresa))
                        .addGap(18, 18, 18)
                        .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jtfNombreEmpresa, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                            .addComponent(jftfCuitEmpresaContratante, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                            .addComponent(jtfTipoServicio)
                            .addComponent(jtfDireccionEmpresa))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jpnlRegEmpresaContratanteLayout.setVerticalGroup(
            jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlRegEmpresaContratanteLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblNombreEmpresaContratante)
                    .addComponent(jtfNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblCuitEmpresaContratante)
                    .addComponent(jftfCuitEmpresaContratante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblTipoServicio)
                    .addComponent(jtfTipoServicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblDireccionEmpresa)
                    .addComponent(jtfDireccionEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlRegEmpresaContratanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jbtnCancelar)
                        .addComponent(jbtnRegEmpresaContratante))
                    .addComponent(jlblAviso))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jpnlRegEmpresaContratante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(235, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jpnlRegEmpresaContratante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(123, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jbtnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnCancelarActionPerformed
        principal.exit();
    }//GEN-LAST:event_jbtnCancelarActionPerformed

    private void jbtnRegEmpresaContratanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnRegEmpresaContratanteActionPerformed
        if( checkBlankCampos() ){
            Long cuit = Long.parseLong(jftfCuitEmpresaContratante.getText());
            EmpresaContratante emp = new EmpresaContratante( cuit, jtfNombreEmpresa.getText(), jtfDireccionEmpresa.getText(), jtfTipoServicio.getText() );
            try {
                empresa.addEmpresaContratante(emp);
            } catch (EmpresaException ex) {
                Logger.getLogger(VentanaRegEmpresaContratante.class.getName()).log(Level.SEVERE, null, ex);
            }
            blankCampos();
        }else{
            jlblAviso.setText("Complete todos los campos");
            jlblAviso.setVisible(true);
        }
    }//GEN-LAST:event_jbtnRegEmpresaContratanteActionPerformed

    private boolean checkBlankCampos(){
        if( !jtfNombreEmpresa.getText().isBlank() ){
            if( !jftfCuitEmpresaContratante.getText().isBlank() ){
                if( !jtfTipoServicio.getText().isBlank() ){
                    if( !jtfDireccionEmpresa.getText().isBlank() ){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private void blankCampos(){
        jtfNombreEmpresa.setText("");
        jftfCuitEmpresaContratante.setText("");
        jtfTipoServicio.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jbtnCancelar;
    private javax.swing.JButton jbtnRegEmpresaContratante;
    private javax.swing.JFormattedTextField jftfCuitEmpresaContratante;
    private javax.swing.JLabel jlblAviso;
    private javax.swing.JLabel jlblCuitEmpresaContratante;
    private javax.swing.JLabel jlblDireccionEmpresa;
    private javax.swing.JLabel jlblNombreEmpresaContratante;
    private javax.swing.JLabel jlblTipoServicio;
    private javax.swing.JPanel jpnlRegEmpresaContratante;
    private javax.swing.JTextField jtfDireccionEmpresa;
    private javax.swing.JTextField jtfNombreEmpresa;
    private javax.swing.JTextField jtfTipoServicio;
    // End of variables declaration//GEN-END:variables
}
