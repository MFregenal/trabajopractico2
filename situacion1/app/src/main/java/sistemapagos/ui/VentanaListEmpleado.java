package sistemapagos.ui;

import javax.swing.DefaultListModel;
import sistemapagos.clases.Empresa;
import sistemapagos.clases.Empleado;
import sistemapagos.clases.EmpleadoAsalariado;
import sistemapagos.clases.EmpleadoComisionado;

public class VentanaListEmpleado extends javax.swing.JPanel {

    private Empresa empresa;
    private UIPrincipal principal;
    
    public VentanaListEmpleado(Empresa empresa, UIPrincipal principal) {
        this.empresa = empresa;
        this.principal = principal;
        initComponents();
        listarEmpleados();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnlListarEmpleados = new javax.swing.JPanel();
        jlblBuscarEmpleado = new javax.swing.JLabel();
        jftfBuscarEmpleado = new javax.swing.JFormattedTextField();
        jbtnBuscarEmpleado = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jspListadoEmpleados = new javax.swing.JScrollPane();
        jListadoEmpleados = new javax.swing.JList<>();
        jspInfEmpleado = new javax.swing.JScrollPane();
        jtaInfEmpleado = new javax.swing.JTextArea();
        jbtnSalirListEmpleados = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jpnlListarEmpleados.setBorder(javax.swing.BorderFactory.createTitledBorder("Listar Empleados"));
        jpnlListarEmpleados.setPreferredSize(new java.awt.Dimension(398, 359));

        jlblBuscarEmpleado.setText("Buscar: ");

        try {
            jftfBuscarEmpleado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jbtnBuscarEmpleado.setText("Buscar");
        jbtnBuscarEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnBuscarEmpleadoActionPerformed(evt);
            }
        });

        jLabel1.setText("DNI, Nombre, Apellido");

        jListadoEmpleados.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jListadoEmpleados.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListadoEmpleadosValueChanged(evt);
            }
        });
        jspListadoEmpleados.setViewportView(jListadoEmpleados);

        jtaInfEmpleado.setColumns(20);
        jtaInfEmpleado.setRows(5);
        jspInfEmpleado.setViewportView(jtaInfEmpleado);

        jbtnSalirListEmpleados.setText("Salir");
        jbtnSalirListEmpleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnSalirListEmpleadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnlListarEmpleadosLayout = new javax.swing.GroupLayout(jpnlListarEmpleados);
        jpnlListarEmpleados.setLayout(jpnlListarEmpleadosLayout);
        jpnlListarEmpleadosLayout.setHorizontalGroup(
            jpnlListarEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlListarEmpleadosLayout.createSequentialGroup()
                .addGroup(jpnlListarEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlListarEmpleadosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jpnlListarEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jbtnBuscarEmpleado)
                            .addGroup(jpnlListarEmpleadosLayout.createSequentialGroup()
                                .addComponent(jlblBuscarEmpleado)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jftfBuscarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlListarEmpleadosLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jpnlListarEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jbtnSalirListEmpleados)
                            .addComponent(jspInfEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)))
                .addGroup(jpnlListarEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jspListadoEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
        );
        jpnlListarEmpleadosLayout.setVerticalGroup(
            jpnlListarEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlListarEmpleadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnlListarEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblBuscarEmpleado)
                    .addComponent(jftfBuscarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnlListarEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlListarEmpleadosLayout.createSequentialGroup()
                        .addComponent(jbtnBuscarEmpleado)
                        .addGap(18, 18, 18)
                        .addComponent(jspInfEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                        .addComponent(jbtnSalirListEmpleados))
                    .addComponent(jspListadoEmpleados))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(133, Short.MAX_VALUE)
                .addComponent(jpnlListarEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(99, 99, 99))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jpnlListarEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(189, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jbtnBuscarEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnBuscarEmpleadoActionPerformed
        DefaultListModel modelo = new DefaultListModel();
        String a = "No se encontro el empleado";
        modelo.addElement(a);
        jListadoEmpleados.setEnabled(false);
        if( !jftfBuscarEmpleado.getText().isBlank() ){
            if( jftfBuscarEmpleado.getText().length() == 8 ){
                if(!empresa.getEmpleados().isEmpty()){
                    Empleado empleado = empresa.returnEmpleado( Integer.parseInt(jftfBuscarEmpleado.getText()) );
                    if(empleado != null){
                        modelo.removeAllElements();
                        modelo.addElement( empleado.getDni() + ", " + empleado.getNombreEmpleado() + ", " + empleado.getApellidoEmpleado() );
                        jListadoEmpleados.setEnabled(true);
                    }
                }
            }
        }
        jListadoEmpleados.removeAll();
        jListadoEmpleados.setModel(modelo);
        jftfBuscarEmpleado.setText("");
    }//GEN-LAST:event_jbtnBuscarEmpleadoActionPerformed

    private void jListadoEmpleadosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListadoEmpleadosValueChanged
        jtaInfEmpleado.setText("");
        Integer i = jListadoEmpleados.getSelectedIndex();
        Object empleado = empresa.getEmpleados().get(i);
        if( empleado instanceof EmpleadoAsalariado ){
            jtaInfEmpleado.append("Nombre: " + ((EmpleadoAsalariado) empleado).getNombreEmpleado() + "\n");
            jtaInfEmpleado.append("Apellido: " + ((EmpleadoAsalariado) empleado).getApellidoEmpleado() + "\n");
            jtaInfEmpleado.append("DNI: " + ((EmpleadoAsalariado) empleado).getDni() + "\n");
            jtaInfEmpleado.append("Domicilio: " + ((EmpleadoAsalariado) empleado).getdomicilio() + "\n");
            jtaInfEmpleado.append("Asignacion Libre: " + ((EmpleadoAsalariado) empleado).getAsignacionLibre() + "\n");
            jtaInfEmpleado.append("Sueldo Base: " + ((EmpleadoAsalariado) empleado).getSueldoBase() + "\n");
        }else{
            if( empleado instanceof EmpleadoComisionado){
                jtaInfEmpleado.append("Nombre: " + ((EmpleadoComisionado) empleado).getNombreEmpleado() + "\n");
                jtaInfEmpleado.append("Apellido: " + ((EmpleadoComisionado) empleado).getApellidoEmpleado() + "\n");
                jtaInfEmpleado.append("DNI: " + ((EmpleadoComisionado) empleado).getDni() + "\n");
                jtaInfEmpleado.append("Domicilio: " + ((EmpleadoComisionado) empleado).getdomicilio() + "\n");
                jtaInfEmpleado.append("Asignacion Libre: " + ((EmpleadoComisionado) empleado).getAsignacionLibre() + "\n");
                jtaInfEmpleado.append("Sueldo Base: " + ((EmpleadoComisionado) empleado).getSueldoBase() + "\n");
            }
        }
    }//GEN-LAST:event_jListadoEmpleadosValueChanged

    private void jbtnSalirListEmpleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnSalirListEmpleadosActionPerformed
        principal.exit();
    }//GEN-LAST:event_jbtnSalirListEmpleadosActionPerformed

    private void listarEmpleados(){
        DefaultListModel modelo = new DefaultListModel();
        String a = "No se encontro el empleado";
        for(Empleado i: empresa.getEmpleados()){
            if( i == null){
                modelo.addElement( a );
            }else{
                modelo.addElement(i.getDni() + ", " + i.getNombreEmpleado() + ", " + i.getApellidoEmpleado());
            }
        }
        jListadoEmpleados.removeAll();
        jListadoEmpleados.setModel(modelo);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList<String> jListadoEmpleados;
    private javax.swing.JButton jbtnBuscarEmpleado;
    private javax.swing.JButton jbtnSalirListEmpleados;
    private javax.swing.JFormattedTextField jftfBuscarEmpleado;
    private javax.swing.JLabel jlblBuscarEmpleado;
    private javax.swing.JPanel jpnlListarEmpleados;
    private javax.swing.JScrollPane jspInfEmpleado;
    private javax.swing.JScrollPane jspListadoEmpleados;
    private javax.swing.JTextArea jtaInfEmpleado;
    // End of variables declaration//GEN-END:variables
}
