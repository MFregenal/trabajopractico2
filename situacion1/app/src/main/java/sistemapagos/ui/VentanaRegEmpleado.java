package sistemapagos.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import sistemapagos.clases.Empleado;
import sistemapagos.clases.EmpleadoAsalariado;
import sistemapagos.clases.Empresa;
import sistemapagos.clases.EmpleadoComisionado;
import sistemapagos.excepciones.EmpresaException;

public class VentanaRegEmpleado extends javax.swing.JPanel {
    private Empresa empresa;
    UIPrincipal principal;
    
    public VentanaRegEmpleado(Empresa empresa, UIPrincipal principal) {
        this.empresa = empresa;
        this.principal = principal;
        initComponents();
        jlblAviso.setVisible(false);
        blankCampos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlAsalariado = new javax.swing.JPanel();
        btnRegistrarEmpleado = new javax.swing.JButton();
        jftfSalarioEmpleado = new javax.swing.JFormattedTextField();
        jtfDomicilioEmpleado = new javax.swing.JTextField();
        jtfApellidoEmpleado = new javax.swing.JTextField();
        jtfNombreEmpleado = new javax.swing.JTextField();
        lblDomicilioEmpleado = new javax.swing.JLabel();
        lblDniEmpleado = new javax.swing.JLabel();
        lblNombreEmpleado1 = new java.awt.Label();
        jcbTipoEmpleado = new javax.swing.JComboBox<>();
        lblTipoEmpleado = new java.awt.Label();
        lblApellidoEmpleado = new java.awt.Label();
        lblTitulo = new java.awt.Label();
        jftfDniEmpleado = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();
        btnSalirReg = new javax.swing.JButton();
        jlblAviso = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        pnlAsalariado.setBorder(javax.swing.BorderFactory.createTitledBorder("Registrar Empleado"));

        btnRegistrarEmpleado.setText("Registrar");
        btnRegistrarEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarEmpleadoActionPerformed(evt);
            }
        });

        jftfSalarioEmpleado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        jftfSalarioEmpleado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jftfSalarioEmpleadoKeyTyped(evt);
            }
        });

        lblDomicilioEmpleado.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblDomicilioEmpleado.setText("Domicilio: ");

        lblDniEmpleado.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblDniEmpleado.setText("D.N.I: ");

        lblNombreEmpleado1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblNombreEmpleado1.setText("Nombre: ");

        jcbTipoEmpleado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Asalariado", "Comisionado" }));
        jcbTipoEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoEmpleadoActionPerformed(evt);
            }
        });

        lblTipoEmpleado.setText("Tipo Empleado: ");

        lblApellidoEmpleado.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblApellidoEmpleado.setText("Apellido:");

        lblTitulo.setAlignment(java.awt.Label.CENTER);
        lblTitulo.setText("Empleado Asalariado");

        try {
            jftfDniEmpleado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel1.setText("Salario:");

        btnSalirReg.setText("Salir");
        btnSalirReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirRegActionPerformed(evt);
            }
        });

        jlblAviso.setForeground(new java.awt.Color(255, 0, 0));
        jlblAviso.setText("jLabel2");

        javax.swing.GroupLayout pnlAsalariadoLayout = new javax.swing.GroupLayout(pnlAsalariado);
        pnlAsalariado.setLayout(pnlAsalariadoLayout);
        pnlAsalariadoLayout.setHorizontalGroup(
            pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAsalariadoLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAsalariadoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnRegistrarEmpleado)
                        .addGap(20, 20, 20)
                        .addComponent(btnSalirReg)))
                .addContainerGap())
            .addGroup(pnlAsalariadoLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlblAviso)
                    .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlAsalariadoLayout.createSequentialGroup()
                            .addComponent(lblNombreEmpleado1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                            .addComponent(jtfNombreEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlAsalariadoLayout.createSequentialGroup()
                                .addComponent(lblTipoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jcbTipoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlAsalariadoLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(pnlAsalariadoLayout.createSequentialGroup()
                                        .addComponent(lblApellidoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(26, 26, 26)
                                        .addComponent(jtfApellidoEmpleado, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAsalariadoLayout.createSequentialGroup()
                                        .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblDniEmpleado)
                                            .addComponent(lblDomicilioEmpleado)
                                            .addComponent(jLabel1))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jftfSalarioEmpleado, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                                            .addComponent(jtfDomicilioEmpleado)
                                            .addComponent(jftfDniEmpleado))))))))
                .addGap(0, 121, Short.MAX_VALUE))
        );
        pnlAsalariadoLayout.setVerticalGroup(
            pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAsalariadoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblTipoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbTipoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtfNombreEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNombreEmpleado1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblApellidoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfApellidoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDniEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jftfDniEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfDomicilioEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDomicilioEmpleado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jftfSalarioEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlAsalariadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRegistrarEmpleado)
                    .addComponent(btnSalirReg)
                    .addComponent(jlblAviso))
                .addGap(56, 56, 56))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(110, Short.MAX_VALUE)
                .addComponent(pnlAsalariado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(122, 122, 122))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(pnlAsalariado, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(218, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jftfSalarioEmpleadoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jftfSalarioEmpleadoKeyTyped
        char c = evt.getKeyChar();
        if( c < '0' || c > '9') evt.consume();
    }//GEN-LAST:event_jftfSalarioEmpleadoKeyTyped

    private void jcbTipoEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTipoEmpleadoActionPerformed
       if( jcbTipoEmpleado.getModel().getSelectedItem().equals("Asalariado") ){
           lblTitulo.setText("Empleado Asalariado");
       }else{
            lblTitulo.setText("Empleado Comisionado");
       }
    }//GEN-LAST:event_jcbTipoEmpleadoActionPerformed

    private void btnRegistrarEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarEmpleadoActionPerformed
        if( checkBlankCampos() ){
            if( jcbTipoEmpleado.getModel().getSelectedItem().equals("Asalariado") ){
                Integer dni = Integer.parseInt(jftfDniEmpleado.getText());
                Double salario = Double.parseDouble(jftfSalarioEmpleado.getText());
                Empleado emp = new EmpleadoAsalariado(jtfNombreEmpleado.getText(), jtfApellidoEmpleado.getText(), dni, jtfDomicilioEmpleado.getText(), salario);
                try {
                    empresa.addEmpleado(emp);
                } catch (EmpresaException ex) {
                    Logger.getLogger(VentanaRegEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                Empleado emp = new EmpleadoComisionado(jtfNombreEmpleado.getText(), jtfApellidoEmpleado.getText(), Integer.parseInt(jftfDniEmpleado.getText()), jtfDomicilioEmpleado.getText(), Double.parseDouble(jftfSalarioEmpleado.getText()));
                try {
                    empresa.addEmpleado(emp);
                } catch (EmpresaException ex) {
                    Logger.getLogger(VentanaRegEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            blankCampos();
        }else{
            jlblAviso.setText("Complete todos los campos");
            jlblAviso.setVisible(true);
        }
    }//GEN-LAST:event_btnRegistrarEmpleadoActionPerformed

    private void btnSalirRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirRegActionPerformed
        principal.exit();
    }//GEN-LAST:event_btnSalirRegActionPerformed

    private boolean checkBlankCampos(){
        if( !jtfNombreEmpleado.getText().isBlank() ){
            if( !jtfApellidoEmpleado.getText().isBlank() ){
                if( !jftfDniEmpleado.getText().isBlank() ){
                    if( !jtfDomicilioEmpleado.getText().isBlank() ){
                        if( !jftfSalarioEmpleado.getText().isBlank() ){
                            return true;
                        }
                    }
                }
            }
            
        }
        return false;
    }
    
    private void blankCampos(){
        jftfDniEmpleado.setText("");
        jftfSalarioEmpleado.setText("");
        jtfApellidoEmpleado.setText("");
        jtfNombreEmpleado.setText("");
        jtfDomicilioEmpleado.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRegistrarEmpleado;
    private javax.swing.JButton btnSalirReg;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JComboBox<String> jcbTipoEmpleado;
    private javax.swing.JFormattedTextField jftfDniEmpleado;
    private javax.swing.JFormattedTextField jftfSalarioEmpleado;
    private javax.swing.JLabel jlblAviso;
    private javax.swing.JTextField jtfApellidoEmpleado;
    private javax.swing.JTextField jtfDomicilioEmpleado;
    private javax.swing.JTextField jtfNombreEmpleado;
    private java.awt.Label lblApellidoEmpleado;
    private javax.swing.JLabel lblDniEmpleado;
    private javax.swing.JLabel lblDomicilioEmpleado;
    private java.awt.Label lblNombreEmpleado1;
    private java.awt.Label lblTipoEmpleado;
    private java.awt.Label lblTitulo;
    private javax.swing.JPanel pnlAsalariado;
    // End of variables declaration//GEN-END:variables
}
