package sistemapagos.ui;

import sistemapagos.clases.Empresa;

public class VentanaLiquidarSueldosEmpleados extends javax.swing.JPanel {
    
    private Empresa empresa;
    private UIPrincipal principal;
    
    public VentanaLiquidarSueldosEmpleados(Empresa empresa, UIPrincipal principal) {
        this.setSize(630, 580);
        this.empresa = empresa;
        this.principal = principal;
        initComponents();
        iniciarProceso();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnlProgreso = new javax.swing.JPanel();
        jpBarraProgreso = new javax.swing.JProgressBar();
        jlblTextoProgreso = new javax.swing.JLabel();
        jbtnOkProgreso = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(680, 530));

        jpnlProgreso.setBorder(javax.swing.BorderFactory.createTitledBorder("Liquidar Sueldos"));

        jpBarraProgreso.setStringPainted(true);

        jlblTextoProgreso.setText("Realizando los pagos");

        jbtnOkProgreso.setText("Ok");
        jbtnOkProgreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnOkProgresoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnlProgresoLayout = new javax.swing.GroupLayout(jpnlProgreso);
        jpnlProgreso.setLayout(jpnlProgresoLayout);
        jpnlProgresoLayout.setHorizontalGroup(
            jpnlProgresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlProgresoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnlProgresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlProgresoLayout.createSequentialGroup()
                        .addComponent(jlblTextoProgreso)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlProgresoLayout.createSequentialGroup()
                        .addGap(0, 33, Short.MAX_VALUE)
                        .addGroup(jpnlProgresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlProgresoLayout.createSequentialGroup()
                                .addComponent(jpBarraProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(42, 42, 42))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlProgresoLayout.createSequentialGroup()
                                .addComponent(jbtnOkProgreso)
                                .addContainerGap())))))
        );
        jpnlProgresoLayout.setVerticalGroup(
            jpnlProgresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlProgresoLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jlblTextoProgreso)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jpBarraProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jbtnOkProgreso)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(155, 155, 155)
                .addComponent(jpnlProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(159, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(131, 131, 131)
                .addComponent(jpnlProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(226, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jbtnOkProgresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnOkProgresoActionPerformed
        principal.exit();
    }//GEN-LAST:event_jbtnOkProgresoActionPerformed

    private void iniciarProceso(){
        jpBarraProgreso.setValue(0);
        jbtnOkProgreso.setEnabled(false);
        empresa.liquidarSueldos(this);
        jpBarraProgreso.setVisible(false);
        jlblTextoProgreso.setLocation(100, 10);
        jlblTextoProgreso.setText("Liquidacion de Sueldos Exitosa!!");
        jbtnOkProgreso.setEnabled(true);
    }
    
    public void actualizarBarra(int i){
        jpBarraProgreso.setValue(i);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jbtnOkProgreso;
    private javax.swing.JLabel jlblTextoProgreso;
    private javax.swing.JProgressBar jpBarraProgreso;
    private javax.swing.JPanel jpnlProgreso;
    // End of variables declaration//GEN-END:variables
}
