package sistemapagos.ui;

import static java.awt.event.KeyEvent.VK_ENTER;
import sistemapagos.clases.*;

public class UIPrincipal extends javax.swing.JFrame {
    
    private Empresa empresa;
    
    public UIPrincipal() {
        this.setSize(600, 600);
        this.setLocationRelativeTo(null);
        initComponents();
        labelAviso.setVisible(false);
        jMenuBar1.setVisible(false);
        jpnlMiEmpresa.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnlMiEmpresa = new javax.swing.JPanel();
        lbGetNombreEmpresa = new java.awt.Label();
        lbGetCuitEmpresa = new java.awt.Label();
        lbGetDireccionEmpresa = new java.awt.Label();
        btnOkMiEmpresa = new java.awt.Button();
        pnlContenedor = new javax.swing.JPanel();
        panelBienvenida = new javax.swing.JPanel();
        labelBienvenida = new java.awt.Label();
        labelBienvenida2 = new java.awt.Label();
        labelNombreEmpresa = new java.awt.Label();
        labelCuitEmpresa = new java.awt.Label();
        labelDireccionEmpresa = new java.awt.Label();
        textNombreEmpresa = new java.awt.TextField();
        textDireccionEmpresa = new java.awt.TextField();
        jButtonAceptar = new javax.swing.JButton();
        labelAviso = new javax.swing.JLabel();
        textFormatCuitEmpresa = new javax.swing.JFormattedTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuInicio = new javax.swing.JMenu();
        jMenuItemMiEmpresa = new javax.swing.JMenuItem();
        jMenuEmpleados = new javax.swing.JMenu();
        jMenuItemRegEmpleado = new javax.swing.JMenuItem();
        jMenuItemListEmpleado = new javax.swing.JMenuItem();
        jMenuItemLiqSueldos = new javax.swing.JMenuItem();
        jMenuEmpresas = new javax.swing.JMenu();
        jMenuItemRegEmpresaContratante = new javax.swing.JMenuItem();
        jMenuItemListEmpresasContratantes = new javax.swing.JMenuItem();

        jpnlMiEmpresa.setBorder(javax.swing.BorderFactory.createTitledBorder("Mi Empresa"));
        jpnlMiEmpresa.setToolTipText("Datos de la Empresa");

        lbGetNombreEmpresa.setText("label1");

        lbGetCuitEmpresa.setText("label1");

        lbGetDireccionEmpresa.setText("label1");

        btnOkMiEmpresa.setLabel("OK");
        btnOkMiEmpresa.setName(""); // NOI18N
        btnOkMiEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkMiEmpresaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnlMiEmpresaLayout = new javax.swing.GroupLayout(jpnlMiEmpresa);
        jpnlMiEmpresa.setLayout(jpnlMiEmpresaLayout);
        jpnlMiEmpresaLayout.setHorizontalGroup(
            jpnlMiEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlMiEmpresaLayout.createSequentialGroup()
                .addGroup(jpnlMiEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlMiEmpresaLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnOkMiEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpnlMiEmpresaLayout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addGroup(jpnlMiEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbGetCuitEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbGetDireccionEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbGetNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 88, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jpnlMiEmpresaLayout.setVerticalGroup(
            jpnlMiEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlMiEmpresaLayout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addComponent(lbGetDireccionEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbGetCuitEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbGetNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnOkMiEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        pnlContenedor.setBackground(new java.awt.Color(255, 255, 255));

        panelBienvenida.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelBienvenida.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                panelBienvenidaKeyReleased(evt);
            }
        });

        labelBienvenida.setAlignment(java.awt.Label.CENTER);
        labelBienvenida.setText("ˇˇBienvenido al Sistema de Pago!!");

        labelBienvenida2.setText("Como primer paso ingrese los datos de su Empresa.");

        labelNombreEmpresa.setText("Nombre de la Empresa: ");

        labelCuitEmpresa.setText("CUIT: ");

        labelDireccionEmpresa.setText("Direccion: ");

        textNombreEmpresa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textNombreEmpresaKeyReleased(evt);
            }
        });

        textDireccionEmpresa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textDireccionEmpresaKeyReleased(evt);
            }
        });

        jButtonAceptar.setText("Aceptar");
        jButtonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAceptarActionPerformed(evt);
            }
        });
        jButtonAceptar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jButtonAceptarKeyReleased(evt);
            }
        });

        labelAviso.setForeground(new java.awt.Color(255, 0, 0));
        labelAviso.setText("Por favor, complete todos los campos.");

        try {
            textFormatCuitEmpresa.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        textFormatCuitEmpresa.setToolTipText("");
        textFormatCuitEmpresa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textFormatCuitEmpresaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout panelBienvenidaLayout = new javax.swing.GroupLayout(panelBienvenida);
        panelBienvenida.setLayout(panelBienvenidaLayout);
        panelBienvenidaLayout.setHorizontalGroup(
            panelBienvenidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBienvenidaLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButtonAceptar)
                .addGap(61, 61, 61))
            .addGroup(panelBienvenidaLayout.createSequentialGroup()
                .addGroup(panelBienvenidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelBienvenidaLayout.createSequentialGroup()
                        .addGap(213, 213, 213)
                        .addComponent(labelBienvenida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelBienvenidaLayout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addGroup(panelBienvenidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelBienvenidaLayout.createSequentialGroup()
                                .addComponent(labelCuitEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textFormatCuitEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelBienvenidaLayout.createSequentialGroup()
                                .addComponent(labelNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelBienvenidaLayout.createSequentialGroup()
                                .addComponent(labelDireccionEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textDireccionEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(labelAviso)))
                    .addGroup(panelBienvenidaLayout.createSequentialGroup()
                        .addGap(158, 158, 158)
                        .addComponent(labelBienvenida2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(158, Short.MAX_VALUE))
        );
        panelBienvenidaLayout.setVerticalGroup(
            panelBienvenidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBienvenidaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelBienvenida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelBienvenida2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(panelBienvenidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBienvenidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelCuitEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFormatCuitEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBienvenidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelDireccionEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textDireccionEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelAviso)
                .addGap(18, 18, 18)
                .addComponent(jButtonAceptar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlContenedorLayout = new javax.swing.GroupLayout(pnlContenedor);
        pnlContenedor.setLayout(pnlContenedorLayout);
        pnlContenedorLayout.setHorizontalGroup(
            pnlContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContenedorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBienvenida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlContenedorLayout.setVerticalGroup(
            pnlContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContenedorLayout.createSequentialGroup()
                .addGap(0, 79, Short.MAX_VALUE)
                .addComponent(panelBienvenida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 79, Short.MAX_VALUE))
        );

        jMenuInicio.setText("Inicio");

        jMenuItemMiEmpresa.setText("Mi Empresa");
        jMenuItemMiEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemMiEmpresaActionPerformed(evt);
            }
        });
        jMenuInicio.add(jMenuItemMiEmpresa);

        jMenuBar1.add(jMenuInicio);

        jMenuEmpleados.setText("Empleados");

        jMenuItemRegEmpleado.setText("Registrar Empleado");
        jMenuItemRegEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRegEmpleadoActionPerformed(evt);
            }
        });
        jMenuEmpleados.add(jMenuItemRegEmpleado);

        jMenuItemListEmpleado.setText("Listar Empleados");
        jMenuItemListEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemListEmpleadoActionPerformed(evt);
            }
        });
        jMenuEmpleados.add(jMenuItemListEmpleado);

        jMenuItemLiqSueldos.setText("Liquidar Sueldos");
        jMenuItemLiqSueldos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLiqSueldosActionPerformed(evt);
            }
        });
        jMenuEmpleados.add(jMenuItemLiqSueldos);

        jMenuBar1.add(jMenuEmpleados);

        jMenuEmpresas.setText("Empresas");

        jMenuItemRegEmpresaContratante.setText("Registrar Empresa Contratante");
        jMenuItemRegEmpresaContratante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRegEmpresaContratanteActionPerformed(evt);
            }
        });
        jMenuEmpresas.add(jMenuItemRegEmpresaContratante);

        jMenuItemListEmpresasContratantes.setText("Listar Empresas Contratantes");
        jMenuEmpresas.add(jMenuItemListEmpresasContratantes);

        jMenuBar1.add(jMenuEmpresas);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 630, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlContenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 416, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlContenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAceptarActionPerformed
        if(textNombreEmpresa.getText().length() != 0 ){
            if(textFormatCuitEmpresa.getText().length() != 0 ){
                if(textDireccionEmpresa.getText().length() != 0){
                    pnlContenedor.remove(panelBienvenida);
                    jMenuBar1.setVisible(true);
                    Long cuit = Long.parseLong(textFormatCuitEmpresa.getText());
                    empresa = new Empresa(textNombreEmpresa.getText(),  textDireccionEmpresa.getText(), cuit);
                }
            }
        }else{
            labelAviso.setVisible(true);
        }
    }//GEN-LAST:event_jButtonAceptarActionPerformed

    private void jMenuItemMiEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemMiEmpresaActionPerformed
        lbGetNombreEmpresa.setText("Nombre: " + empresa.getNombreEmpresa());
        lbGetCuitEmpresa.setText("CUIT: " + String.valueOf(empresa.getCuitEmpresa()));
        lbGetDireccionEmpresa.setText("Direccion: " + empresa.getDireccionEmpresa());
        jpnlMiEmpresa.setBounds(200, 120, 270, 170);
        jpnlMiEmpresa.setVisible(true);
        cambiarEstadoMenu(false);
        pnlContenedor.add(jpnlMiEmpresa);
        pnlContenedor.revalidate();
        pnlContenedor.repaint();
    }//GEN-LAST:event_jMenuItemMiEmpresaActionPerformed

    private void btnOkMiEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkMiEmpresaActionPerformed
        jpnlMiEmpresa.setVisible(false);
        cambiarEstadoMenu(true);
        pnlContenedor.removeAll();
    }//GEN-LAST:event_btnOkMiEmpresaActionPerformed

    private void jMenuItemRegEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRegEmpleadoActionPerformed
        VentanaRegEmpleado regEmpleado = new VentanaRegEmpleado(empresa, this);
        regEmpleado.setVisible(true);
        regEmpleado.setSize(450, 370);
        regEmpleado.setLocation(100, 20);
        pnlContenedor.add(regEmpleado);
        pnlContenedor.revalidate();
        pnlContenedor.repaint();
        cambiarEstadoMenu(false);
    }//GEN-LAST:event_jMenuItemRegEmpleadoActionPerformed

    private void jMenuItemListEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemListEmpleadoActionPerformed
        VentanaListEmpleado listEmpleado = new VentanaListEmpleado(empresa, this);
        listEmpleado.setVisible(true);
        listEmpleado.setSize(470, 470);
        listEmpleado.setLocation(100, -10);
        pnlContenedor.add(listEmpleado);
        pnlContenedor.revalidate();
        pnlContenedor.repaint();
        cambiarEstadoMenu(false);
    }//GEN-LAST:event_jMenuItemListEmpleadoActionPerformed

    private void jMenuItemLiqSueldosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLiqSueldosActionPerformed
        VentanaLiquidarSueldosEmpleados vlsEmpleados = new VentanaLiquidarSueldosEmpleados(empresa, this);
        vlsEmpleados.setVisible(true);
        vlsEmpleados.setSize(470, 470);
        vlsEmpleados.setLocation(50, 20);
        pnlContenedor.add(vlsEmpleados);
        pnlContenedor.revalidate();
        pnlContenedor.repaint();
        cambiarEstadoMenu(false);
    }//GEN-LAST:event_jMenuItemLiqSueldosActionPerformed

    private void panelBienvenidaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_panelBienvenidaKeyReleased
        if(evt.getExtendedKeyCode() == VK_ENTER){
            jButtonAceptarKeyReleased(evt);
        }
    }//GEN-LAST:event_panelBienvenidaKeyReleased

    private void jButtonAceptarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonAceptarKeyReleased
        jButtonAceptar.doClick();
    }//GEN-LAST:event_jButtonAceptarKeyReleased

    private void textNombreEmpresaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textNombreEmpresaKeyReleased
        if(evt.getExtendedKeyCode() == VK_ENTER){
            jButtonAceptarKeyReleased(evt);
        }
    }//GEN-LAST:event_textNombreEmpresaKeyReleased

    private void textFormatCuitEmpresaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFormatCuitEmpresaKeyReleased
        if(evt.getExtendedKeyCode() == VK_ENTER){
            jButtonAceptarKeyReleased(evt);
        }
    }//GEN-LAST:event_textFormatCuitEmpresaKeyReleased

    private void textDireccionEmpresaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textDireccionEmpresaKeyReleased
        if(evt.getExtendedKeyCode() == VK_ENTER){
            jButtonAceptarKeyReleased(evt);
        }
    }//GEN-LAST:event_textDireccionEmpresaKeyReleased

    private void jMenuItemRegEmpresaContratanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRegEmpresaContratanteActionPerformed
        VentanaRegEmpresaContratante vrEmpresaContratante = new VentanaRegEmpresaContratante(empresa, this);
        vrEmpresaContratante.setVisible(true);
        vrEmpresaContratante.setSize(520, 370);
        vrEmpresaContratante.setLocation(50, 20);
        pnlContenedor.add(vrEmpresaContratante);
        pnlContenedor.revalidate();
        pnlContenedor.repaint();
        cambiarEstadoMenu(false);
    }//GEN-LAST:event_jMenuItemRegEmpresaContratanteActionPerformed

    private void cambiarEstadoMenu(boolean estado){
        jMenuBar1.setEnabled(estado);
        jMenuInicio.setEnabled(estado);
        jMenuEmpleados.setEnabled(estado);
        jMenuEmpresas.setEnabled(estado);
    }
    
    public void exit(){
        pnlContenedor.removeAll();
        pnlContenedor.revalidate();
        pnlContenedor.repaint();
        cambiarEstadoMenu(true);
    }
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UIPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Button btnOkMiEmpresa;
    private javax.swing.JButton jButtonAceptar;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuEmpleados;
    private javax.swing.JMenu jMenuEmpresas;
    private javax.swing.JMenu jMenuInicio;
    private javax.swing.JMenuItem jMenuItemLiqSueldos;
    private javax.swing.JMenuItem jMenuItemListEmpleado;
    private javax.swing.JMenuItem jMenuItemListEmpresasContratantes;
    private javax.swing.JMenuItem jMenuItemMiEmpresa;
    private javax.swing.JMenuItem jMenuItemRegEmpleado;
    private javax.swing.JMenuItem jMenuItemRegEmpresaContratante;
    private javax.swing.JPanel jpnlMiEmpresa;
    private javax.swing.JLabel labelAviso;
    private java.awt.Label labelBienvenida;
    private java.awt.Label labelBienvenida2;
    private java.awt.Label labelCuitEmpresa;
    private java.awt.Label labelDireccionEmpresa;
    private java.awt.Label labelNombreEmpresa;
    private java.awt.Label lbGetCuitEmpresa;
    private java.awt.Label lbGetDireccionEmpresa;
    private java.awt.Label lbGetNombreEmpresa;
    private javax.swing.JPanel panelBienvenida;
    private javax.swing.JPanel pnlContenedor;
    private java.awt.TextField textDireccionEmpresa;
    private javax.swing.JFormattedTextField textFormatCuitEmpresa;
    private java.awt.TextField textNombreEmpresa;
    // End of variables declaration//GEN-END:variables
}
