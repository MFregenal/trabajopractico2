package sistemapagos.excepciones;

public class EmpresaException extends Exception{
    public EmpresaException(String msj){
        super(msj);
    }
}
