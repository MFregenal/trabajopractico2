package sistemapagos.clases;

import java.time.LocalDate;

public class Comision {

    private LocalDate fechaComision;
    private Double valorComision;
    
    public Comision( LocalDate fechaComision, Double valorComision){
        this.fechaComision = fechaComision;
        this.valorComision = valorComision;
    }

    public void setFechaComision(LocalDate fechaComision){
        this.fechaComision = fechaComision;
    }

    public void setValorComision(Double valorComision){
        this.valorComision = valorComision;
    }

    public LocalDate getFechaComision(){
        return fechaComision;
    }

    public Double getValorComision(){
        return valorComision;
    }
}
