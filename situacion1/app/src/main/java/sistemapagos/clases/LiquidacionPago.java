package sistemapagos.clases;

import java.util.ArrayList;
import java.util.List;
import sistemapagos.ui.VentanaLiquidarSueldosEmpleados;

import sistemapagos.excepciones.EmpresaException;

public interface LiquidacionPago{
    
    public static final Double impositiva = 0.2;
    public List<Empleado> empleadosPagadosSI = new ArrayList<>(); //Sin impositiva
    public List<Empleado> empleadosPagadosCI = new ArrayList<>(); //Con impositiva
    public List<PagoEmpresa> pagosPendientesEmpresaContratante = new ArrayList<>();

    public List<Empleado> getEmpleadoPagadosSI(); 
    public List<Empleado> getEmpleadoPagadosCI();
    public void liquidarSueldos(VentanaLiquidarSueldosEmpleados vlsEmpleados);
    public void liquidarPagosEmpresaContratante(long cuitEmpresaContratante) throws EmpresaException;
}