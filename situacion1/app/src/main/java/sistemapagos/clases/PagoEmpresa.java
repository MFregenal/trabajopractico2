package sistemapagos.clases;

public class PagoEmpresa {
    private EmpresaContratante empresaContratante = null;
    private Double pago = 0.0;

    public PagoEmpresa( EmpresaContratante empresaContratante, double pago){
        this.empresaContratante = empresaContratante;
        this.pago = pago;
    }

    // public  void setEmpresaContratante(EmpresaContratante empresaContratante){
    //     this.empresaContratante = empresaContratante;
    // }

    // public void setPago(double pago){
    //     this.pago = pago;
    // }

    public EmpresaContratante getEmpresaContratante(){
        return empresaContratante;
    }
    
    public Double getPago(){
        return pago;
    }
}
