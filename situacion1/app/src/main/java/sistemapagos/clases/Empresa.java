package sistemapagos.clases;

import java.util.ArrayList;
import java.util.List;

import sistemapagos.excepciones.EmpresaException;
import sistemapagos.ui.VentanaLiquidarSueldosEmpleados;

public class Empresa implements LiquidacionPago{

    private String nombreEmpresa;
    private String direccionEmpresa;
    private Long cuitEmpresa;
    private List<Empleado> empleados;
    private List<EmpresaContratante> empresasContratantes;

    public Empresa(String nombreEmpresa, String direccionEmpresa, long cuitEmpresa){
        this.nombreEmpresa = nombreEmpresa;
        this.direccionEmpresa = direccionEmpresa;
        this.cuitEmpresa = cuitEmpresa;
        empleados = new ArrayList<>();
        empresasContratantes = new ArrayList<>();
    }

    public void setNombreEmpresa(String nombre){
        this.nombreEmpresa = nombre;
    }

    public void setDireccionEmpresa(String direccionEmpresa){
        this.direccionEmpresa = direccionEmpresa;
    }

    public String getNombreEmpresa(){
        return this.nombreEmpresa;
    }

    public String getDireccionEmpresa(){
        return this.direccionEmpresa;
    }

    public void setCuitEmpresa(long cuitEmpresa){
        this.cuitEmpresa = cuitEmpresa;
    }

    public Long getCuitEmpresa(){
        return cuitEmpresa;
    }

    public void addEmpleado(Empleado empleado) throws EmpresaException{
        if( returnEmpleado(empleado.getDni()) == null){
            empleados.add(empleado);
        }else{
            throw new EmpresaException("Empleado ya existe");
        }
    }

    public List<Empleado> getEmpleados(){
        return empleados;
    }

    public void addEmpresaContratante(EmpresaContratante empresaContratante) throws EmpresaException{
        if( returnEmpresaContratante(empresaContratante.getCuitEmpresa()) == null ){
            empresasContratantes.add(empresaContratante);
        }else{
            throw new EmpresaException("Empresa Contratante ya existe");
        }
    }

    public List<EmpresaContratante> getEmpresasContratantes(){
        return empresasContratantes;
    }

    public Empleado returnEmpleado( Integer dni){
        Empleado empleado = null;
        for ( Empleado i : empleados ){
            if ( i.getDni().equals(dni)){
                empleado = i;
            }
        }
        return empleado;
    }

    public EmpresaContratante returnEmpresaContratante(long cuitEmpresaContratante){
        EmpresaContratante empresaContratante = null;
        for(EmpresaContratante i : empresasContratantes){
            if( i.getCuitEmpresa().equals(cuitEmpresaContratante)){
                empresaContratante = i;
            }
        }
        return empresaContratante;
    }

    public void modAsignacionLibre(int dni, double asignacionLibre) throws EmpresaException{
        Empleado empleado = returnEmpleado(dni);
        if(empleado != null){
                    empleado.setAsignacionLibre(asignacionLibre);
        }else{
            throw new EmpresaException("Empleado inexistente");
        }
    }

    public void asignarSueldo( int dni, double salario) throws EmpresaException{
        Empleado empleado = returnEmpleado(dni);
        if(empleado != null){
                empleado.setSalario(salario);
        }else{
            throw new EmpresaException("Empleado inexistente");
        }
    }

    public void addPagoEmpresaContratante(EmpresaContratante empresaContratante, double pago){
        PagoEmpresa pagoEmpresa = new PagoEmpresa(empresaContratante, pago);
        pagosPendientesEmpresaContratante.add(pagoEmpresa);
    }

    public List<PagoEmpresa> getPagosPendientesEmpresaContratante(){
        return pagosPendientesEmpresaContratante;
    }

    @Override
    public List<Empleado> getEmpleadoPagadosSI() {
        return empleadosPagadosSI;
    }

    @Override
    public List<Empleado> getEmpleadoPagadosCI() {
        return empleadosPagadosCI;
    }

    @Override
    public void liquidarSueldos(VentanaLiquidarSueldosEmpleados vlsEmpleado) {
        int i=1;
        for ( Empleado empleado : empleados ){
            if ( empleado.getSalario() <= empleado.getAsignacionLibre()){
                empleadosPagadosSI.add(empleado);
            }else{
                empleado.setSalario( empleado.getSalario() - (empleado.getSalario() * impositiva) );
                empleadosPagadosCI.add(empleado);
            }
            vlsEmpleado.actualizarBarra(i);
            i++;
        }
    }

    @Override
    public void liquidarPagosEmpresaContratante(long cuitEmpresaContratante) throws EmpresaException {
        double saldo = 0;
        EmpresaContratante empresaContratante = returnEmpresaContratante(cuitEmpresaContratante);
        for(PagoEmpresa pago : pagosPendientesEmpresaContratante){
            if(pago.getEmpresaContratante().getCuitEmpresa().equals(cuitEmpresaContratante)){
                saldo += pago.getPago();
            }
        }
        if(empresaContratante != null){
            empresaContratante.setSaldo(saldo);
        }else{
            throw new EmpresaException("Error al realizar la liquidacion");
        }
    }
}
