package sistemapagos.clases;

public abstract class Empleado {
    
    private String nombreEmpleado;
    private String apellidoEmpleado;
    private Integer dni;
    private String domicilio;
    private Double asignacionLibre;
    private Double salario;

    public Empleado(String nombreEmpleado, String apellidoEmpleado, Integer dni, String domicilio, Double asignacionLibre){
        this.nombreEmpleado = nombreEmpleado;
        this.apellidoEmpleado = apellidoEmpleado;
        this.dni = dni;
        this.domicilio = domicilio;
        this.asignacionLibre = asignacionLibre;
        this.salario=0.0;
    }

    public void setNombreEmpleado(String nombre){
        this.nombreEmpleado = nombre;
    }
    
    public void setApellidoEmpleado(String apellido){
        this.apellidoEmpleado = apellido;
    }

    public void setDni(Integer dni){
        this.dni = dni;
    }

    public void setDomicilio(String domicilio){
        this.domicilio = domicilio;
    }

    public String getNombreEmpleado(){
        return this.nombreEmpleado;
    }

    public String getApellidoEmpleado(){
        return this.apellidoEmpleado;
    }

    public Integer getDni(){
        return this.dni;
    }

    public String getdomicilio(){
        return this.domicilio;
    }

    public void setSalario(double salario){
        this.salario = salario;
    }

    public Double getSalario(){
        return salario;
    }

    public void setAsignacionLibre(Double asignacionLibre){
        this.asignacionLibre = asignacionLibre;
    }

    public Double getAsignacionLibre(){
        return asignacionLibre;
    }

    public abstract Double calcularSueldo();
}
