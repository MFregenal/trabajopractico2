package sistemapagos.clases;

public class EmpleadoAsalariado extends Empleado{
    private Double sueldoBase;

    public EmpleadoAsalariado(String nombreEmpleado, String apellidoEmpleado, Integer dni, String domicilio, double sueldoBase){
        super(nombreEmpleado, apellidoEmpleado, dni, domicilio, sueldoBase);
        this.sueldoBase = sueldoBase;
    }

    public void setSueldoBase(double sueldoBase){
        this.sueldoBase = sueldoBase;
    }

    public double getSueldoBase(){
        return sueldoBase;
    }

    public Double calcularSueldo(){
        return sueldoBase;
    }
}
