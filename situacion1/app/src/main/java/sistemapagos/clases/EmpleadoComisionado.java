package sistemapagos.clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class EmpleadoComisionado extends Empleado{

    private Double sueldoBase;
    private static final Double multComision = 1.5;
    private List<Comision> comisiones;
    
    public EmpleadoComisionado(String nombreEmpleado, String apellidoEmpleado, Integer dni, String domicilio, double sueldoBase){
        super(nombreEmpleado, apellidoEmpleado, dni, domicilio, sueldoBase);
        this.sueldoBase = sueldoBase;
        comisiones = new ArrayList<>();
    }

    public void setSueldoBase(Double sueldoBase){
        this.sueldoBase = sueldoBase;
    }

    public Double getSueldoBase(){
        return sueldoBase;
    }

    public void addComision(LocalDate fechaComision, double valorComision){
        Comision comision = new Comision(fechaComision, valorComision);
        comisiones.add(comision);
    }

    public List<Comision> getComisiones(){
        return comisiones;
    }

    public Double sumarComisiones(){
        double totalComision = 0;
        for (Comision comision : comisiones){
            totalComision += comision.getValorComision() * multComision;
        }
        return totalComision;
    }

    public Double calcularSueldo(){
        double sueldototal = 0;
        sueldototal = sueldoBase + sumarComisiones();
        return sueldototal;
    }
}
