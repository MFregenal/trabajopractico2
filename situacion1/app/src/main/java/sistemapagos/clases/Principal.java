package sistemapagos.clases;

import java.util.List;
import sistemapagos.ui.*;

public class Principal {

    public static void main(String[] args){

        UIPrincipal principal = new UIPrincipal();
        principal.setVisible(true);

//        Empresa empresa = new Empresa("Arcor", "Av. Misiones 2022", 20-8888888-8);
//        EmpleadoAsalariado empleado1 = new EmpleadoAsalariado("Micaela", "Gordillo", 39962536, "Av. Guemes 1231", 62000.0);
//        EmpleadoComisionado empleado2 = new EmpleadoComisionado("Nicolas", "Perez", 36585621, "9 Julio 1238", 37000.0);
//        EmpresaContratante empresaContratante = new EmpresaContratante(66598, "Empresa 1", "servicio 1");
//        try {
//            empresa.addEmpleado(empleado1);
//        } catch (Exception e) {
//            System.out.println("Error: " + e);
//        }
//        try {
//            empresa.addEmpleado(empleado2);
//        } catch (Exception e) {
//            System.out.println("Error: " + e);
//        }
//        try {
//            empresa.addEmpleado(empleado2);
//        } catch (Exception e) {
//            System.out.println("Error: " + e);
//        }
//        try {
//            empresa.asignarSueldo(36585621, 50000);
//        } catch (Exception e) {
//            System.out.println("Error: " + e);
//        }
//        try {
//            empresa.asignarSueldo(39962536, 100000);
//        } catch (Exception e) {
//            System.out.println("Error: " + e);
//        }
//        try {
//            empresa.modAsignacionLibre(36585621, 50000);
//        } catch (Exception e) {
//            System.out.println("Error: " + e);
//        }
//        empresa.liquidarSueldos();
//        
//        System.out.println("\nEmpleados con Impositiva");
//        System.out.println("----------------------------");
//        List<Empleado> empleadosSI = empresa.getEmpleadoPagadosCI();
//        for(Empleado empleado : empleadosSI){
//            System.out.println("Nombre: " + empleado.getNombreEmpleado());
//            System.out.println("Apellido: " + empleado.getApellidoEmpleado());
//            System.out.println("DNI: " + empleado.getDni());
//            System.out.println("Domicilio: " + empleado.getdomicilio());
//            System.out.println("Salario: " + empleado.getSalario());
//        }
//        
//
//        System.out.println("\nEmpleados sin Impositiva");
//        System.out.println("------------------------------");
//        List<Empleado> empleadosCI = empresa.getEmpleadoPagadosSI();
//        for(Empleado empleado : empleadosCI){
//            System.out.println("Nombre: " + empleado.getNombreEmpleado());
//            System.out.println("Apellido: " + empleado.getApellidoEmpleado());
//            System.out.println("DNI: " + empleado.getDni());
//            System.out.println("Domicilio: " + empleado.getdomicilio());
//            System.out.println("Salario: " + empleado.getSalario());
//        }
//        try{
//            empresa.addEmpresaContratante(empresaContratante);
//        }catch(Exception e){
//            System.out.println("Error: " + e);
//        }
//        empresa.addPagoEmpresaContratante(empresa.returnEmpresaContratante(66598), 6500);
//        empresa.addPagoEmpresaContratante(empresa.returnEmpresaContratante(66598), 6500);
//        empresa.addPagoEmpresaContratante(empresa.returnEmpresaContratante(66598), 6500);
//        try{
//            empresa.liquidarPagosEmpresaContratante(66598);
//        }catch(Exception e){
//            System.out.println("Error: " + e);
//        }
//        System.out.println("\nEmpresas Contratantes");
//        System.out.println("--------------------------------");
//        for(EmpresaContratante emp : empresa.getEmpresasContratantes()){
//            System.out.println("Nombre empresa: " + emp.getNombreEmpresaContratante());
//            System.out.println("Cuit: " + emp.getCuitEmpresaContratante());
//            System.out.println("Tipo de servicio: " + emp.getTipoServicio());
//            System.out.println("Saldo: " + emp.getSaldo());
//        }
    }
}
