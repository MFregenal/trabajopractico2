package sistemapagos.clases;

public class EmpresaContratante extends Empresa{

    private String tipoServicio;
    private Double saldo;

    public EmpresaContratante(long cuitEmpresaContratante, String nombreEmpresaContratante, String direccionEmpresaContratante, String tipoServicio){
        super(nombreEmpresaContratante, direccionEmpresaContratante, cuitEmpresaContratante);
        this.tipoServicio = tipoServicio;
        this.saldo = 0.0;
    }

    public void setSaldo( double saldo){
        this.saldo = saldo;
    }

    public Double getSaldo(){
        return saldo;
    }

    public String getTipoServicio(){
        return tipoServicio;
    }
}
