import static org.junit.Assert.assertEquals;

import org.junit.Test;

import sistemapagos.clases.*;

public class EmpresaTest {

    @Test
    public void checkAddEmpleado(){
        Empresa empresa = new Empresa("Subaru", "Argentia 3320", 287778889);
        Empleado empleado = new EmpleadoAsalariado("Juan", "Diaz", 40111222, "Peru 123", 15000);
        Empleado empleado2 = new EmpleadoComisionado("Jorge", "Nieva", 40333444, "Chile 345", 20000);
        try{
            empresa.addEmpleado(empleado);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.addEmpleado(empleado2);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.addEmpleado(empleado2);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        assertEquals(empleado2, empresa.returnEmpleado(40333444));
    }

    @Test
    public void checkAddEmpresaContratante(){
        Empresa empresa = new Empresa("Subaru", "Argentia 3320", 287778889);
        EmpresaContratante empresaC = new EmpresaContratante(289996662, "Stich", "Misiones 123", "Informatico");
        EmpresaContratante empresaC2 = new EmpresaContratante(28666333, "Coin", "Misiones 123", "Contable");
        try{
            empresa.addEmpresaContratante(empresaC);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.addEmpresaContratante(empresaC2);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.addEmpresaContratante(empresaC2);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        assertEquals(empresaC2, empresa.returnEmpresaContratante(28666333));
    }

    @Test
    public void checkModAsignacionLibre(){
        Double asignacion = 25000.0;
        Empresa empresa = new Empresa("Subaru", "Argentia 3320", 287778889);
        Empleado empleado = new EmpleadoAsalariado("Juan", "Diaz", 40111222, "Peru 123", 15000);
        Empleado empleado2 = new EmpleadoComisionado("Jorge", "Nieva", 40333444, "Chile 345", 20000);
        try{
            empresa.addEmpleado(empleado);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.addEmpleado(empleado2);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.addEmpleado(empleado2);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.modAsignacionLibre(40333444, 25000);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        assertEquals(asignacion, empresa.returnEmpleado(40333444).getAsignacionLibre());
    }

    @Test
    public void checkAsignarSueldo(){
        Double sueldo = 20000.0;
        Empresa empresa = new Empresa("Subaru", "Argentia 3320", 287778889);
        Empleado empleado = new EmpleadoAsalariado("Juan", "Diaz", 40111222, "Peru 123", 15000);
        Empleado empleado2 = new EmpleadoComisionado("Jorge", "Nieva", 40333444, "Chile 345", 20000);
        try{
            empresa.addEmpleado(empleado);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.addEmpleado(empleado2);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.addEmpleado(empleado2);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.asignarSueldo(40333444, 20000);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        assertEquals(sueldo, empresa.returnEmpleado(40333444).getSalario());
    }

    @Test
    public void checkAddPagoEmpresaContratante(){
        Empresa empresa = new Empresa("Subaru", "Argentia 3320", 287778889);
        EmpresaContratante empresaC = new EmpresaContratante(289996662, "Stich", "Misiones 123", "Informatico");
        try{
            empresa.addEmpresaContratante(empresaC);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        empresa.addPagoEmpresaContratante(empresaC, 10000);
        assertEquals(false, empresa.getPagosPendientesEmpresaContratante().isEmpty());
    }

    @Test
    public void checkLiquidarSueldos(){
        Empresa empresa = new Empresa("Subaru", "Argentia 3320", 287778889);
        Empleado empleado = new EmpleadoAsalariado("Juan", "Diaz", 40111222, "Peru 123", 15000);
        try{
            empresa.addEmpleado(empleado);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        try{
            empresa.modAsignacionLibre(40111222, 20000);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        empresa.liquidarSueldos(null);
        assertEquals(false, empresa.getEmpleadoPagadosSI().isEmpty());
    }
    
    @Test
    public void checkLiquidarPagosEmpresaContratante(){
        Double saldo = 20000.0;
        Empresa empresa = new Empresa("Subaru", "Argentia 3320", 287778889);
        EmpresaContratante empresaC = new EmpresaContratante(289996662, "Stich", "Misiones 123", "Informatico");
        try{
            empresa.addEmpresaContratante(empresaC);
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
        empresa.addPagoEmpresaContratante(empresaC, 10000);
        empresa.addPagoEmpresaContratante(empresaC, 10000);
        try {
            empresa.liquidarPagosEmpresaContratante(289996662);            
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        assertEquals(saldo, empresa.returnEmpresaContratante(289996662).getSaldo());
    }
}
