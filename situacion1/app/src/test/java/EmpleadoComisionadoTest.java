import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;
import sistemapagos.clases.*;

public class EmpleadoComisionadoTest {
    
    @Test
    public void checkSumarComisiones(){
        Double resultado = 3000.0 * 1.5;
        EmpleadoComisionado empleadoC = new EmpleadoComisionado("Juan", "Diaz", 40888555, "Peru 123", 15000);
        empleadoC.addComision(LocalDate.now(), 1000);
        empleadoC.addComision(LocalDate.now(), 1000);
        empleadoC.addComision(LocalDate.now(), 1000);
        assertEquals(resultado, empleadoC.sumarComisiones());
    }

    @Test
    public void checkCalcularSueldo(){
        Double resultado = 15000 + 3000.0 * 1.5;
        EmpleadoComisionado empleadoC = new EmpleadoComisionado("Juan", "Diaz", 40888555, "Peru 123", 15000);
        empleadoC.addComision(LocalDate.now(), 1000);
        empleadoC.addComision(LocalDate.now(), 1000);
        empleadoC.addComision(LocalDate.now(), 1000);
        assertEquals(resultado, empleadoC.calcularSueldo());
    }
}
